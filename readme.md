# ZURB WebApp Template

[![devDependency Status](https://david-dm.org/zurb/foundation-zurb-template/dev-status.svg)](https://david-dm.org/zurb/foundation-zurb-template#info=devDependencies)

**Please open all issues with this template on the main [Ilyo Sites](https://bitbucket.org/stevew_cc/bootstrap4-gulp/issues?status=new&status=open) repo.**

This is the official Bootstrap 4. We use this template at Ilyo to deliver static code to our clients. It has a Gulp-powered build system with these features:

- Handlebars HTML templates with Panini
- Sass compilation and prefixing
- JavaScript module bundling with webpack
- Built-in BrowserSync server
- For production builds:
  - CSS compression
  - HTML Compression
  - Inline SVGs
  - JavaScript module bundling with webpack
  - Image compression

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (Version 6 or greater recommended, tested with 6.11.4 and 8.12.0)
- [Git](https://git-scm.com/)

This template can be installed with the Foundation CLI, or downloaded and set up manually.

### Using the CLI

Install the CLI with this command:

```bash
npm install
```
For production run
```bash
gulp build
```

For Development run
```bash
gulp default
```


NGINX Setup 
```bash
gzip on;
gzip_disable "MSIE [1-6]\\.(?!.*SV1)";
gzip_proxied any;
gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript image/x-icon image/bmp image/svg+xml application/javascript application/js;
gzip_vary on;

# Expire rules for static content

# cache.appcache, your document html and data
location ~* \.(?:manifest|appcache|html?|xml|json)$ {
	expires -1;
	# access_log logs/static.log; # I don't usually include a static log
}

# Feed
location ~* \.(?:rss|atom)$ {
	expires 1h;
	add_header Pragma public;
	add_header Cache-Control "public";
}

# Media: images, icons, video, audio, HTC
location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc|webp)$ {
	expires 1M;
	access_log off;
	add_header Pragma public;
	add_header Cache-Control "public";
}

# CSS and Javascript
location ~* \.(?:css|js)$ {
	expires 1y;
	access_log off;
	add_header Pragma public;
	add_header Cache-Control "public";
}
if ($ssl_protocol = "") {
	rewrite ^/(.*) https://$server_name/$1 permanent;
}
```
Finally, run `yarn start` to run Gulp. Your finished site will be created in a folder called `dist`, viewable at this URL:

```
http://localhost:8000
```

To create compressed, production-ready assets, run `yarn run build`.
